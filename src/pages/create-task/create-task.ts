import { Task } from './../../models/task.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-create-task',
  templateUrl: 'create-task.html',
})
export class CreateTaskPage {

  taskName: string = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private alertController: AlertController
  ) { }

  createTask(): void {

    if (this.taskName === null || this.taskName.trim() === '') {
      this.alertController.create({
        title: 'Error',
        message: 'Task cannot be empty',
        buttons: ['ok']
      }).present();
      return;
    }

    this.storage.get('tasks').then(tasks => {
      const task = new Task(Date.now(), this.taskName, false);
      const taskArray: Task[] = tasks === null ? [] : JSON.parse(tasks);
      taskArray.push(task);
      this.storage.set('tasks', JSON.stringify(taskArray));
      this.navCtrl.pop()
    });
  }

}
