import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { findIndex } from 'lodash';

import { Task } from './../../models/task.model';

import { CreateTaskPage } from './../create-task/create-task';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  tasks: Task[];

  constructor(
    public navCtrl: NavController,
    private storage: Storage
  ) {

  }

  ionViewWillEnter(): void {
    this.loadTasks();
  }

  deleteTask(taskId: number): void {
    const index = findIndex(this.tasks, {id: taskId});
    this.tasks.splice(index, 1);
    this.storage.set('tasks', JSON.stringify(this.tasks));
  }

  updateTask(task: Task): void {
    task.isDone = !task.isDone;
    const index = findIndex(this.tasks, {id: task.id});
    this.tasks[index] = task;
    this.storage.set('tasks', JSON.stringify(this.tasks));
  }

  openCreateTasksPage(): void {
    this.navCtrl.push(CreateTaskPage);
  }

  private loadTasks(): void {
    this.storage.get('tasks').then(tasks => this.tasks = JSON.parse(tasks));
  }

}
