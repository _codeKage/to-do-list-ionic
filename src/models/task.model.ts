export class Task {
    id: number;
    taskName: string;
    isDone: boolean;

    constructor(id: number, taskname: string, isDone: boolean) {
        this.id = id;
        this.taskName = taskname;
        this.isDone = isDone;
    }
}